package com.eco_skout;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

public class Slider extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slider);

        setTitle("Eco-Skout");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            moveTaskToBack(true);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.nav_events) {
            startActivity(new Intent(Slider.this, Events.class));
        } else if (id == R.id.nav_profile) {
            startActivity(new Intent(Slider.this, ProfileActivity.class));
        } else if (id == R.id.nav_achieve) {
            startActivity(new Intent(Slider.this, Achievements.class));
        } else if (id == R.id.nav_notifs) {
            startActivity(new Intent(Slider.this, Notifs.class));
        } else if (id == R.id.nav_contact) {
            startActivity(new Intent(Slider.this, Contact.class));
        } else if (id == R.id.nav_policy) {
            startActivity(new Intent(Slider.this, Policy.class));
        } else if (id == R.id.nav_about) {
            startActivity(new Intent(Slider.this, About.class));
        } else if (id == R.id.nav_logout) {
            startActivity(new Intent(Slider.this, Login.class));
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
