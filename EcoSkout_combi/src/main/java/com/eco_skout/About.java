package com.eco_skout;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class About extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        //toolbar
        setTitle("About");
        Toolbar toolbar = (Toolbar) findViewById(R.id.es_toolbar);
        setSupportActionBar(toolbar);
        //up button
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        StringBuilder text = new StringBuilder();
        BufferedReader br = null;

        try {
            br = new BufferedReader(new InputStreamReader(getAssets().open("about.txt")));

            String fileLine;
            while ((fileLine = br.readLine()) != null) {
                text.append(fileLine);
                text.append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (br != null) {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        TextView tv = (TextView) findViewById(R.id.about_txt);
        tv.setText((CharSequence) text);
    }
}
