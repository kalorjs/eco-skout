package com.eco_skout;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewSwitcher;

public class Login extends AppCompatActivity {

    ImageSwitcher is;
    int count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //GUYS IN-ADD KO TO KASI DEFAULT THEME YUNG SA SPLASH, NAKA-DECLARE SA MANIFEST. TO
        // CHANGE BACK, ETO GINAMIT KO SA BABA:
        setTheme(R.style.AppTheme);
        //
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //Password visibility toggle icon
        is = (ImageSwitcher) findViewById(R.id.visibility_btn);
        is.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
             public View makeView() {
                 ImageView iv = new ImageView(getApplicationContext());
                 return iv;
             }
         });
        is.setImageResource(R.drawable.ic_visibility_black_24dp);
        is.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count++;
                if(count%2 == 0) {
                    is.setImageResource(R.drawable.ic_visibility_black_24dp);
                }
                else {
                    is.setImageResource(R.drawable.ic_visibility_off_black_24dp);
                }
            }
        });

        //handle Events activity
        Button login = (Button) findViewById(R.id.loginBtn);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Login.this, Slider.class));
                finish();
            }
        });

        //handle Registration activity
        TextView reg = (TextView) findViewById(R.id.register_btn);
        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Login.this, Options.class));
            }
        });

        //handle ForgotPassword activity
        TextView forgPass = (TextView) findViewById(R.id.forgotBtn);
        forgPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Login.this, ForgotPassword.class));
            }
        });
    }

    //to avoid continuous incrementing and app crashing
    @Override
    protected void onPause() {
        super.onPause();
        count = 0;
    }
}
