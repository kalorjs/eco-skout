package com.eco_skout;

import android.app.DatePickerDialog;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.support.v7.widget.*;
import android.view.View;
import android.widget.*;
import android.widget.DatePicker;
import android.widget.Toolbar;

import java.text.DateFormat;
import java.util.Calendar;

public class CamperReg extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    ImageButton dateBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camper_reg);
        //toolbar
        setTitle("Register");
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.es_toolbar);
        setSupportActionBar(toolbar);
        //up button
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        dateBtn = (ImageButton) findViewById(R.id.date_btn);
        dateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment datePicker = new com.eco_skout.DatePicker();
                datePicker.show(getSupportFragmentManager(), "date picker");
            }
        });
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, day);
        c.set(Calendar.YEAR, year);
        String pickedDate = DateFormat.getDateInstance(DateFormat.SHORT).format(c.getTime());

        TextView tv = (TextView) findViewById(R.id.input_date);
        tv.setText(pickedDate);
    }
}
