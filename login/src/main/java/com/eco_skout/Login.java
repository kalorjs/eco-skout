package com.eco_skout;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.ViewSwitcher;

public class Login extends AppCompatActivity {

    ImageSwitcher is;
    int count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        is = (ImageSwitcher) findViewById(R.id.visibility_btn);
        is.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
             public View makeView() {
                 ImageView iv = new ImageView(getApplicationContext());
                 return iv;
             }
         });
        is.setImageResource(R.drawable.ic_visibility_black_24dp);
        is.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count++;
                if(count%2 == 0) {
                    is.setImageResource(R.drawable.ic_visibility_black_24dp);
                }
                else {
                    is.setImageResource(R.drawable.ic_visibility_off_black_24dp);
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        count = 0;
    }
}
