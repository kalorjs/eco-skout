package com.eco_skout;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.StringBuilder;

public class ServerResponseGetter
{
	public static String getResponse(HttpURLConnection urlConnection) throws Exception {
		InputStream is=null;
		StringBuilder sb=new StringBuilder();
		try {
			is=urlConnection.getInputStream();
			InputStreamReader irs=new InputStreamReader(is);
			BufferedReader br=new BufferedReader(irs);
			String line="";
			while((line=br.readLine()) != null) {
				sb.append(line);
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		finally {
			urlConnection.disconnect();
		}
		return sb.toString();
	}
}
