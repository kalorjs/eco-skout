package com.eco_skout;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Button;

public class Events extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.AppTheme);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_events);

		TextView evtTitle = (TextView) findViewById(R.id.event_title);
		evtTitle.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(Events.this, Details.class));
			}
		});
		Button btn=new Button(this);
		btn.setOnClickListener(new IntentSwitcher(this,CreateEvent.class,null));
	}
}
