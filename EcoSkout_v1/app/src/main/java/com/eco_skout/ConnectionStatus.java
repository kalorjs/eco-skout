package com.eco_skout;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class ConnectionStatus
{
	Context context;

	public ConnectionStatus(Context context) {
		this.context = context;
	}

	public boolean isConnected() {
		ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		boolean isConn = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
		return(isConn);
	}
}
