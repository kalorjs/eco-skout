package com.eco_skout;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.Activity;
import android.widget.LinearLayout;
import android.widget.EditText;
import android.widget.Button;
import android.content.Context;
import android.view.View;
import android.text.InputType;
import android.widget.TextView;
import android.text.TextWatcher;
import android.text.Editable;
import android.widget.PopupWindow;
import android.util.DisplayMetrics;
import android.content.Intent;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.view.ViewGroup;
import android.text.Editable;
import java.util.ArrayList;
import android.view.View.OnFocusChangeListener;

public class OrgReg2 extends AppCompatActivity
{
	Popup popUp=null;
	private static int count=0;
	ArrayList<EditText> ets=new ArrayList<EditText>();
	private static StringBuilder details=new StringBuilder();
	boolean showned=false;

	public ArrayList<EditText> getTextFields(View v) {
		ViewGroup rootView=(ViewGroup)v;
		for(int i=0; i<rootView.getChildCount(); i++) {
			View v1=rootView.getChildAt(i);
			if(v1 instanceof ViewGroup) {
				getTextFields(v1);
				System.out.println("VIEWGROUP");
			}
			System.out.println("VIEW");
			if(v1 instanceof EditText) {
				EditText et=(EditText)v1;
				ets.add(et);
				System.out.println(et.getId());
			}
		}
		return ets;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.AppTheme);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_org_reg2);
		Bundle bund=getIntent().getExtras();
		final String cn=bund.getString("cn");
		System.out.println("cn: " + cn);

		final String ca=bund.getString("ca");
		final String ce=bund.getString("ce");
		final String cp=bund.getString("cp");
		final String cw=bund.getString("cw");

		popUp=new Popup(this,Login.class);
		final EditText oFname=(EditText)findViewById(R.id.org_fName);
		final EditText oLname=(EditText)findViewById(R.id.org_lName);
		final EditText oMob=(EditText)findViewById(R.id.org_number);
		final EditText oEmail=(EditText)findViewById(R.id.org_email);
		// final EditText oPw1=(EditText)findViewById(R.id.org_pw1);
		// final EditText oPw2=(EditText)findViewById(R.id.org_pw2);
		// details.append(cn+"/");
		// details.append(ca+"/");
		// details.append(ce+"/");
		// details.append(cp+"/");
		// details.append(cw+"/");
		// final EditText fName=(EditText)findViewById(R.id.input_fName);
		// final EditText lName=(EditText)findViewById(R.id.input_lName);
		// final EditText bDate=(EditText)findViewById(R.id.input_date);
		// final ImageButton dateBtn=(ImageButton)findViewById(R.id.date_btn);
		// final RadioButton male=(RadioButton)findViewById(R.id.male);
		// final RadioButton female=(RadioButton)findViewById(R.id.female);
		// final EditText mobile=(EditText)findViewById(R.id.input_number);
		// final EditText email=(EditText)findViewById(R.id.input_email);
		// final EditText password=(EditText)findViewById(R.id.input_pw1);
		// final EditText password2=(EditText)findViewById(R.id.input_pw2);
		final Button submitBtn=(Button)findViewById(R.id.submit_btn);
		// final RadioGroup radioGroup=(RadioGroup)findViewById(R.id.radioGroup);
		submitBtn.setEnabled(false);
		ArrayList<EditText> ets=getTextFields(getWindow().getDecorView());
		final int size=ets.size();
		System.out.println("Fucking size: "+size);

		for(int i=0; i<ets.size(); i++) {
			final EditText et=ets.get(i);
			et.setOnFocusChangeListener(new OnFocusChangeListener() {
				public void onFocusChange(View v,boolean hasFocus) {
					if(!hasFocus && !et.getText().toString().matches("")) {
						System.out.println(et.getText().toString());
						count++;
						System.out.println("Focus out");
					}
				}
			});
		}

		oEmail.addTextChangedListener(new TextWatcher(){
			public void afterTextChanged(Editable s) {
				if(s.length()>0) {
					count++;
					System.out.println(count);
					if(count>=size) {
						System.out.println("BUTTON ENABLED");
						submitBtn.setEnabled(true);
					}
				}
			}

			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
		});

		submitBtn.setOnClickListener(new View.OnClickListener() {
			StringBuilder message=new StringBuilder();
			boolean flag=false;
			public void onClick(View v) {
				System.out.println("CLICKED!!");
				String ofn=oFname.getText().toString();
				String oln=oLname.getText().toString();
				String omob=oMob.getText().toString();
				String oea=oEmail.getText().toString();
				boolean isEmailValid=EmailAddressValidator.isValid(oea);
				// boolean passwordsMatch=PasswordValidator.isMatch(pass,pass2);
				if(!isEmailValid) {
					message.append("Invalid Email/");
					flag=true;
				}
				if(!flag) {
					String[] details={"register",ofn,oln,omob,oea,"","","organizer",cn,ca,ce,cw,cp};
					new HttpPOSTClient(EcoIP.ip+"register",popUp).execute(details);
				}
				else {
					message.append("ERROR");
					popUp.setMessage(message);
					if(!showned) {
						popUp.showPopUp();
						showned=true;
					}
					System.out.println(message.toString());
					flag=false;
				}
				message.setLength(0);
				message=new StringBuilder();
				showned=false;
			}
		});
	}
}
