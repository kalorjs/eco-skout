package com.eco_skout;

import android.widget.PopupWindow;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Button;
import android.view.Gravity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.app.Activity;
import android.graphics.Color;
import android.view.View;
import android.graphics.Typeface;
import android.content.Context;
import android.content.Intent;

public class Popup
{
	PopupWindow popUp;
	LinearLayout layout;
	// TextView tv;
	StringBuilder message=new StringBuilder();
	LayoutParams params;
	Button but;
	boolean click=true;
	Context cont=null;
	boolean isShown=false;
	Intent intent=null;
	// int popupConeter;
	// boolean shown=null;

	public Popup(Context c,Class<?> cl) {
		cont=c;
		if(cl!=null) {
			intent=new Intent(cont.getApplicationContext(),cl);
		}
	}

	public String getMessage() {
		return message.toString();
	}

	public void setMessage(StringBuilder s) {
		message=s;
	}

	public boolean getIsShown() {
		return isShown;
	}

	public void setIsShown(boolean i) {
		isShown=i;
	}

	public PopupWindow showPopUp() {
		if(!isShown) {
			DisplayMetrics dm=new DisplayMetrics();
			((Activity)cont).getWindowManager().getDefaultDisplay().getMetrics(dm);
			int width=dm.widthPixels;
			int height=dm.heightPixels;
			LinearLayout layout=new LinearLayout(cont);
			layout.setOrientation(LinearLayout.VERTICAL);
			LinearLayout.LayoutParams lp=new LinearLayout.LayoutParams((int)(width/3),LinearLayout.LayoutParams.WRAP_CONTENT);
			layout.setLayoutParams(lp);
			LayoutParams params=new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
			final PopupWindow popUp=new PopupWindow(layout,(int)(width*.9),(int)(height*.5));
			String m=message.toString();
			final String[] messages=m.split("/");
			final int size=messages.length;
			final TextView[] myTextViews=new TextView[size];
			System.out.println("ETO PO"+messages[size-1]);
			for(int i=size-1; i>=0; i--) {
				System.out.println("popup"+messages[i]);
				TextView tv=new TextView(cont);
				System.out.println("1");
				tv.setText(i==(size-1)?messages[i]:messages[i]+"\n");
				tv.setGravity(Gravity.CENTER);
				layout.addView(tv);
				tv.setTextColor(Color.BLACK);
				if(i==size-1) {
					tv.setTypeface(tv.getTypeface(),Typeface.BOLD);
				}
			}
			final Button but=new Button(cont);
			but.setText("OK");
			// but.setWidth((int)width*.3);
			but.setGravity(Gravity.CENTER);
			layout.setOrientation(LinearLayout.VERTICAL);
			layout.setPadding(20,20,20,20);
			layout.addView(but);
			layout.setBackgroundColor(Color.WHITE);
			popUp.setContentView(layout);
			popUp.showAtLocation(layout,Gravity.CENTER,0,0);

			but.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					popUp.dismiss();
					isShown=false;
					System.out.println(messages[size-1]);
					if("SUCCESS".matches(messages[size-1])) {
						try {
							cont.startActivity(intent);
						}
						catch(Exception e) {
							e.printStackTrace();
						}
					}
				}
			});
		}
		return popUp;
	}
	
	public void setPopupHeader(TextView v, LinearLayout ll,String txt) {
		v.setText(txt.matches("ERROR")==true?"ERROR\n\n\n":"SUCESS\n\n\n");
		v.setGravity(Gravity.CENTER);
		ll.addView(v);
		v.setTextColor(Color.BLACK);
	}
}
