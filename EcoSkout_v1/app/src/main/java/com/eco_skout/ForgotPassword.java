package com.eco_skout;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.text.InputType;
import android.content.Context;

public class ForgotPassword extends AppCompatActivity 
{
	Popup popUp = null;
	ConnectionStatus connStatus;
	boolean isPopupShowned = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.AppTheme);
		super.onCreate(savedInstanceState);
		popUp = new Popup(this,ForgotPass2.class);
		connStatus = new ConnectionStatus(this);
		final Context ctx = this;
		setContentView(R.layout.activity_forgot_password);

		final EditText email = (EditText)findViewById(R.id.email);
		email.setHint("Enter Email");
		email.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);

		final Button subEmail = (Button) findViewById(R.id.submitEmail_btn);
		subEmail.setOnClickListener(new View.OnClickListener() {
			StringBuilder message = new StringBuilder();
			boolean flag = false;
			@Override
			public void onClick(View v) {
				// startActivity(new Intent(ForgotPassword.this, ForgotPass2.class));
				String ea = email.getText().toString();
				boolean isEmailValid = Validator.isEmailValid(ea);
				System.out.println("email :"+ ea);
				System.out.println("isEmailValid: " + isEmailValid);
				if(!isEmailValid) {
					message.append("Invalid email!/");
					flag = true;
				}
				if(!connStatus.isConnected()) {
					message.append("Please check your Internet connection and try again/");
					flag = true;
				}
				if(!flag) {
					String[] credential={"reset", ea};
					// new HttpPOSTResetPassword("https://eco-skout.appspot.com/resetpassword", popUp, ctx).execute(credential);
					// new HttpPOSTClient(protocol+"://"+server+":"+port+"/resetpassword",popUp, ctx).execute(credential);
					// new HttpPOSTResetPassword("http://192.168.137.1:8080/resetpassword", popUp, ctx).execute(credential);
					new HttpPOSTResetPassword(EcoIP.ip+"resetpassword", popUp, ctx).execute(credential);
				}
				else {
					message.append("ERROR");
					popUp.setMessage(message);
					if(!isPopupShowned) {
						popUp.showPopUp();
						isPopupShowned = true;
					}
					flag = false;
				}
				message.setLength(0);
				message = new StringBuilder();
				isPopupShowned = false;
			}
		});
	}
}
