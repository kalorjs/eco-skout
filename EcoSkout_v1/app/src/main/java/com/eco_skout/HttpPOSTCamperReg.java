package com.eco_skout;

import android.os.AsyncTask;
import java.net.HttpURLConnection;
import java.net.URL;
import android.widget.TextView;
import org.json.JSONObject;
import android.util.JsonWriter;
import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import android.content.Intent;
import android.content.Context;

public class HttpPOSTCamperReg extends AsyncTask<String, Void, String>
{
	Popup popUp = null;
	String webApi = null;
	Context context = null;

	public HttpPOSTCamperReg(String w, Popup p, Context fp) {
		webApi = w;
		popUp = p;
		context = fp;
	}

	protected String doInBackground(String... str) {
		HttpURLConnection urlConnection = null;
		String responseData = null;
		try {
			URL url = new URL(webApi);
			urlConnection = (HttpURLConnection)url.openConnection();
			urlConnection.setDoOutput(true);
			urlConnection.setChunkedStreamingMode(0);
			OutputStream os = new BufferedOutputStream(urlConnection.getOutputStream());
			JsonWriter jWriter = new JsonWriter(new OutputStreamWriter(os,"UTF-8"));
			System.out.println("SERVERRRRRR");
			String[] fieldNames={"","fName","lName","mobile","email","password","bDate","sex","uType"};
					jWriter.beginObject();
					for(int i=1; i<str.length; i++) {
						System.out.println("writing"+fieldNames[i]+" as "+ str[i]);
						jWriter.name(fieldNames[i]).value(str[i]);
						System.out.println("written");
					}
					jWriter.endObject();
					jWriter.close();
			responseData = ServerResponseGetter.getResponse(urlConnection);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return responseData;
	}

	protected void onPostExecute(String result) {
		System.out.println("RESPONSE: "+result);
		StringBuilder resultText = new StringBuilder();
		try {
			JSONObject jObj = new JSONObject(result);
			System.out.println((String)jObj.get("result"));
			switch((String)jObj.get("result")) {
				case "success":
					System.out.println("login successfull");
					context.startActivity(new Intent(context, Events.class));
					break;
				case "error":
					resultText.append((String)jObj.get("message")+"/");
					resultText.append("ERROR");
					popUp.setMessage(resultText);
					popUp.showPopUp();
					System.out.println(resultText.toString());
					break;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
}
