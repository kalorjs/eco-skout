package com.eco_skout;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewSwitcher;
import android.widget.EditText;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.Editable;
import android.content.Context;

public class Login extends AppCompatActivity {

	Popup popUp = null;
	ConnectionStatus connStatus;
	ImageSwitcher is;
	int count;
	boolean showned = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		//GUYS IN-ADD KO TO KASI DEFAULT THEME YUNG SA SPLASH, NAKA-DECLARE SA MANIFEST. TO
		// CHANGE BACK, ETO GINAMIT KO SA BABA:
		setTheme(R.style.AppTheme);
		super.onCreate(savedInstanceState);
		popUp = new Popup(this,Events.class);
		connStatus = new ConnectionStatus(this);
		final Context ctx = this;
		setContentView(R.layout.activity_login);
		final EditText email = (EditText)findViewById(R.id.email);
		email.setHint("Enter Email");
		email.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
		final EditText password = (EditText)findViewById(R.id.password);
		password.setHint("Enter Password");
		password.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_PASSWORD);
		final Button loginBtn = (Button)findViewById(R.id.loginBtn);
		loginBtn.setText("Login");
		loginBtn.setEnabled(false);
		// TextView regLink = (TextView)findViewById(R.id.reglink);
		// regLink.setOnClickListener(new IntentSwitcher(this,RegisterOptions.class,null));

		//Password visibility toggle icon
		is = (ImageSwitcher) findViewById(R.id.visibility_btn);
		is.setFactory(new ViewSwitcher.ViewFactory() {
			@Override
			public View makeView() {
				ImageView iv = new ImageView(getApplicationContext());
				return iv;
			}
		});
		is.setImageResource(R.drawable.ic_visibility_black_24dp);
		is.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				count++;
				if(count%2 == 0) {
					is.setImageResource(R.drawable.ic_visibility_black_24dp);
					password.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_PASSWORD);
				}
				else {
					is.setImageResource(R.drawable.ic_visibility_off_black_24dp);
					password.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_NORMAL);
				}
			}
		});

		password.addTextChangedListener(new TextWatcher() {
			public void afterTextChanged(Editable s) {
				if(s.length() > 7 && email.getText().length() > 7) {
					loginBtn.setEnabled(true);
				}
				else {
					loginBtn.setEnabled(false);
				}
				System.out.println(s.length());
				System.out.println("Email to:"+email.getText().toString());
				System.out.println("password to:"+password.getText().toString());
			}
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
		});

		//handle Events activity
		// Button login = (Button) findViewById(R.id.loginBtn);
		loginBtn.setOnClickListener(new View.OnClickListener() {
			StringBuilder message=new StringBuilder();
			boolean flag=false;
			@Override
			public void onClick(View v) {
				// startActivity(new Intent(Login.this, Events.class));
				String ea = email.getText().toString();
				String pw = password.getText().toString();
				// boolean isEmailValid = EmailAddressValidator.isValid(ea);
				// boolean isPasswordValid = PasswordValidator.isValid(pw);
				boolean isEmailValid = Validator.isEmailValid(ea);
				boolean isPasswordValid = Validator.isPasswordValid(pw);
				System.out.println("email :"+ ea);
				System.out.println("password :"+ pw);
				System.out.println("isEmailValid: " + isEmailValid);
				System.out.println("isPasswordValid: " + isPasswordValid);
				if(!isEmailValid || !isPasswordValid) {
					message.append("Invalid email or password/");
					flag=true;
				}
				if(!connStatus.isConnected()) {
					message.append("Please check your Internet connection and try again/");
					flag=true;
				}
				if(!flag) {
					String[] credential={"login", ea, pw};
					// new HttpPOSTLogin("https://eco-skout.appspot.com/login", popUp, ctx).execute(credential);
					// new HttpPOSTLogin(protocol+"://"+server+":"+port+"/login",popUp, ctx).execute(credential);
					new HttpPOSTClient(EcoIP.ip+"login", popUp).execute(credential);
				}
				else {
					message.append("ERROR");
					popUp.setMessage(message);
					if(!showned) {
						popUp.showPopUp();
						showned=true;
					}
					flag=false;
				}
				message.setLength(0);
				message=new StringBuilder();
				showned=false;
			}
		});

		//handle Registration activity
		TextView reg = (TextView) findViewById(R.id.register_btn);
		reg.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(Login.this, Options.class));
			}
		});

		//handle ForgotPassword activity
		TextView forgPass = (TextView) findViewById(R.id.forgotBtn);
		forgPass.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(Login.this, ForgotPassword.class));
			}
		});
	}

	//to avoid continuous incrementing and app crashing
	@Override
	protected void onPause() {
		super.onPause();
		count = 0;
	}
}
