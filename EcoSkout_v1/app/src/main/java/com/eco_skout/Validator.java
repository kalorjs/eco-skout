package com.eco_skout;

public class Validator
{
	public static boolean isEmailValid(String email) {
		if(email.isEmpty()) {
			return(false);
		}
		if(email.matches(".*[\\s].*")) {
			return(false);
		}
		if(email.matches(".*[!#$%^&*+=].*")) {
			return(false);
		}
		if(!email.contains("@")) {
			return(false);
		}
		String domain = email.split("@")[1];
		if(!domain.contains(".")) {
			return(false);
		}
		return(true);
	}

	public static boolean isPasswordValid(String password) {
		int complexityCount = 0;
		if(password.isEmpty()) {
			return(false);
		}
		if(password.length() < 8) {
			return(false);
		}
		if(password.matches(".*[a-z].*")) {
			complexityCount++;
		}
		if(password.matches(".*[A-Z].*")) {
			complexityCount++;
		}
		if(password.matches(".*[0-9].*")) {
			complexityCount++;
		}
		if(password.matches(".*[\\W].*")) {
			complexityCount++;
		}
		if(complexityCount > 2) {
			complexityCount = 0;
			return(true);
		}
		complexityCount = 0;
		return(false);
	}

	public static boolean arePasswordsMatch(String password1, String password2) {
		if(password1.equals(password2)) {
			return(true);
		}
		return(false);
	}

	public boolean isNameValid(String fn, String ln) {
		//check if name is in correct format
		if(fn.isEmpty() || ln.isEmpty()) {
			return(false);
		}
		if(!fn.matches("[A-Za-z ]*")) {
			return(false);
		}
		if(!ln.matches("[A-Za-z ]*")) {
			return(false);
		}
		return(true);
	}

	public boolean isCompanyDetailsValid(String fn, String ln) {
		//check if the name sis in correct format?
		return(true);
	}

	public boolean isMobileValid(String mobile) {
		//check the length of the mobile number
		if(!mobile.matches("[0-9]*")) {
			return(false);
		}
		if(mobile.length() <= 15 && mobile.length() >= 6) {
			return(true);
		}
		return(false);
	}

	public boolean isSexSelected(String sex) {
		//check if either of the 2 radio buttons are selected
		if(sex != null) {
			return(true);
		}
		return(false);
	}

	public boolean checkNullness(String... str) {
		boolean flag = true;
		for(int i = 0; i < str.length; i++) {
			if(str[i] == null || str[i] == "") {
				flag = false;
			}
		}
		return(flag);
	}
}
