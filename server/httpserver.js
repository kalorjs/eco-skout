const http = require('http');
const mongo = require('mongojs');
const querystring = require('querystring');
const sha1 = require('sha1');
const nodemailer = require('nodemailer');
const crypto = require('crypto');
// const user = "skout";
// const pass = "ecoSkout01";
// const host = "35.236.87.1";
// const port = "27017";
const dbas = "ecoskoutdb";
// let esdb = mongo(user+":"+pass+"@"+host+":"+port+"/"+dbas);
let esdb = mongo(dbas);
let handlers = {};
let clients = {};

var transporter = nodemailer.createTransport({
	service: 'gmail',
	auth: {
		user: 'ecoskout@netgies.com',
		pass: 'eco-skout'
	}
});

handlers["/resetpassword"] = (req, res) => {
	if(req.method == "POST") {
		res.writeHead(200,{"Content-Type":"application/json"});
		let data = "";
		req.on('data', function(dd) {
			data += dd.toString();
		});
		req.on('end', function() {
			let msg = JSON.parse(data);
			console.log("email: " + msg.email);
			esdb.users.findOne({ "email":msg.email }, (err,docs) => {
				if(docs != null) {
					let random = crypto.randomBytes(3).toString("hex") + "Es-1";
					console.log("random: " + random);
					let password = sha1(random);
					console.log("temp password: " + password);
					var mailOptions = {
						from: 'ecoskout@netgies.com',
						to: msg.email,
						subject: 'Eco-Skout password reset',
						html: '<h1>Hello!</h1><p>You are receiving this email because you have requested to reset your password. <br>Here is your temporary password to Eco-Skout App:</p><h3>' + random + '</h><br><p>For security, change password as soon as you login to Eco-Skout App.</p> <br><br><h5>Enjoy the camp!</h5>' 
					};
					transporter.sendMail(mailOptions, function(error, info) {
						if(error) {
							console.log(error);
						}
						else {
							console.log('Email sent: ' + info.response);
						}
					});
					res.end(JSON.stringify({ "result":"success", "message":"A temporary password has been sent to your email account." }));
					console.log("A temporary password has been sent to your email account.");
					esdb.users.update(
						{ "email":msg.email },
						{
							$set: { "password":password }
						}
					)
				}
				else if(docs == null){
					res.end(JSON.stringify({"result":"error","message":"Email not found!"}));
					console.log("Email not found!");
				}
			});
		});
	}
}

handlers["/login"] = (req,res) => {
	console.log("someone is logging in...");
	if(req.method == "POST") {
		res.writeHead(200, {"Content-Type":"application/json"});
		let data = "";
		req.on('data', function(dd) {
			data += dd.toString();
		});
		req.on('end', function() {
			let msg = JSON.parse(data);
			console.log(msg);
			esdb.users.findOne({"email":msg.email, "password":sha1(msg.password)}, (err,docs) => {
				if(docs != null) {
					let sid = crypto.randomBytes(16).toString("hex");
					clients[sid] = msg.email;
					res.end(JSON.stringify({"result":"success", "message":"Logged in successfully.", "sid":sid}));
					console.log(`Back to client ${sid}`);
				}
				else if(docs == null){
					res.end(JSON.stringify({"result":"error","message":"Incorrect email or password"}));
					console.log("Error");
				}
			});
		});
	}
}

handlers["/logout"] = (req,res) => {
	console.log("someone is logging out...");
	if(req.method == "POST") {
		res.writeHead(200, {"Content-Type":"application/json"});
		let data = "";
		req.on('data', function(dd) {
			data += dd.toString();
		});
		req.on('end', function() {
			let msg = JSON.parse(data);
			console.log(msg);
			if(clients[msg.sid]) {
				delete clients[msg.sid];
			}
			esdb.users.update(
				{ "email":msg.email },
				{
					$set: { "sid":"" }
				}
			)
			res.end(JSON.stringify({"result":"success","message":"bye"}));
			console.log("bye");
		});
	}
}

handlers["/register"]=(req,res)=> {
	res.writeHead(200,{"Content-Type":"application/json"});
	let data="";
	req.on('data',(dd)=> {
		data+=dd.toString();
	});
	req.on('end',()=> {
		let details=JSON.parse(data);
		console.log(details);
		let password=sha1(details.password);
		esdb.users.findOne({"email":details.email},(err, user)=> {
			if(!err) {
				if(!user) {
					let sid = crypto.randomBytes(16).toString("hex");
					esdb.users.insert({
						"email":details.email,
						"password":password,
						"fname":details.fName,
						"lname":details.lName,
						"bdate":details.bDate,
						"sex":details.sex,
						"mobile":details.mobile,
						"user_type":"camper",
						"sid":sid
					});
					clients[sid] = details.email;
					res.end(JSON.stringify({"result":"success", "message":"Registered successfully.", "sid":sid}));
					console.log(`Back to client ${sid}`);
				}
				else {
					res.end(JSON.stringify({"result":"error","message":"Email or mobile is already taken"}));
					console.log("Email taken");
				}
			}
			else {
				console.log(`Mongodb Error: ${err}`);
			}
		});
	});
}

handlers["/createevent"]=(req,res)=> {
	res.writeHead(200,{"Content-Type":"application/json"});
	let data="";
	req.on('data',(dd)=> {
		data+=dd.toString();
	});
	req.on('end',()=> {
		let details=JSON.parse(data);
		console.log(details);
		if(clients[sid]) {
			// create event
			// organizer
			// date
			// venue
			// details
		}
	});
}

http.createServer((req,res) => {
	if(handlers[req.url]) {
		handlers[req.url](req,res);
	}
	else {
		if(req.url.includes("?")) {
			let url = req.url.split("?");
			let url2 = querystring.parse(req.url);
			handlers[url[0]](req,res);
		}
		else {
			res.writeHead(404, {"Content-Type":"text/html"});
			res.write("<h1>Error 404: Page not found</h1>");
			res.end();
		}
	}
}).listen(8080);
console.log("HTTP server running on port 8080..");
