package com.eco_skout;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.EditText;
import android.widget.Button;
import android.content.Context;
import android.view.View;
import android.text.InputType;
import android.widget.TextView;
import android.text.TextWatcher;
import android.text.Editable;
import android.widget.PopupWindow;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.ViewSwitcher;

public class Login extends Activity {
	Popup popUp = null;
	ConnectionStatus connStatus;
	ImageSwitcher is;
	int count;
	boolean showned=false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		popUp = new Popup(this);
		connStatus = new ConnectionStatus(this);
		setContentView(R.layout.main);
		final EditText email=(EditText)findViewById(R.id.email);
		email.setHint("Enter Email");
		email.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
		final EditText password=(EditText)findViewById(R.id.password);
		password.setHint("Enter Password");
		password.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_PASSWORD);
		final Button loginBtn=(Button)findViewById(R.id.loginBtn);
		loginBtn.setText("Login");
		loginBtn.setEnabled(false);
		TextView regLink=(TextView)findViewById(R.id.reglink);
		regLink.setOnClickListener(new IntentSwitcher(this,RegisterOptions.class,null));
		// regLink.setOnClickListener(new IntentSwitcher(this,OrganizerRegistration.class));
		is=(ImageSwitcher) findViewById(R.id.visibility_btn);

		is.setFactory(new ViewSwitcher.ViewFactory() {
			@Override
			public View makeView() {
				ImageView iv=new ImageView(getApplicationContext());
				return iv;
			}
		});

		is.setImageResource(R.drawable.ic_visibility_black_24dp);

		is.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				count++;
				if(count%2 == 0) {
					is.setImageResource(R.drawable.ic_visibility_black_24dp);
					password.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_PASSWORD);
				}
				else {
					is.setImageResource(R.drawable.ic_visibility_off_black_24dp);
					password.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_NORMAL);
				}
			}
		});

		// regLink.setOnClickListener(new ButtonClicker(this,CamperRegistration.class));
		// regLink.setOnClickListener(new ButtonClicker(this,OrganizerRegistration.class));

		password.addTextChangedListener(new TextWatcher() {
			public void afterTextChanged(Editable s) {
				if(s.length() > 7 && email.getText().length() != 0) {
					loginBtn.setEnabled(true);
				}
				if((s.length() == 0 && email.getText().length() != 0) || (s.length() != 0 && email.getText().length() == 0)) {
					loginBtn.setEnabled(false);
				}
				System.out.println(s.length());
				System.out.println("Email to:"+email.getText().toString());
				System.out.println("password to:"+password.getText().toString());
			}

			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
		});

		loginBtn.setOnClickListener(new View.OnClickListener() {
			StringBuilder message=new StringBuilder();
			boolean flag=false;
			public void onClick(View v) {
				// System.out.println("test");
				String ea = email.getText().toString();
				String pw = password.getText().toString();
				boolean isEmailValid = EmailAddressValidator.isValid(ea);
				boolean isPasswordValid = PasswordValidator.isValid(pw);
				System.out.println("email :"+ ea);
				System.out.println("password :"+ pw);
				System.out.println("isEmailValid: " + isEmailValid);
				System.out.println("isPasswordValid: " + isPasswordValid);
				if(!isEmailValid || !isPasswordValid) {
					message.append("Invalid email or password/");
					flag=true;
				}
				if(!connStatus.isConnected()) {
					message.append("Please check your Internet connection and try again/");
					flag=true;
				}
				if(!flag) {
					String[] credential={"login",ea,pw};
					new HttpPOSTClient(EcoIP.ip+"login",popUp).execute(credential);
				}
				else {
					message.append("ERROR");
					popUp.setMessage(message);
					if(!showned) {
						popUp.showPopUp();
						showned=true;
					}
					flag=false;
				}
				message.setLength(0);
				message=new StringBuilder();
				showned=false;
			}
		});
	}
}
