package com.eco_skout;

import android.content.Context;
import android.util.Log;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;

public class MySQLiteHelper extends SQLiteOpenHelper
{
	public static final String TABLE_EVENTS = "events";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_EVENT = "event";
	public static final String COLUMN_DATE = "date";
	public static final String COLUMN_LOCATION = "location";
	public static final String COLUMN_ORGANIZER = "organizer";
	public static final String COLUMN_DESCRIPTION = "description";
	public static final String COLUMN_NOTE = "note";
	public static final String COLUMN_IMAGE = "image";
	private static final String DATABASE_NAME = "events.db";
	private static final int DATABASE_VERSION = 1;

	private static final String DATABASE_CREATE = "create table " +
		TABLE_EVENTS + "(" + COLUMN_ID +
		" integer primary key autoincrement, " + COLUMN_EVENT +
		" text not null, " + COLUMN_DATE +
		" text not null, " + COLUMN_LOCATION +
		" text not null, " + COLUMN_ORGANIZER +
		" text not null, " + COLUMN_DESCRIPTION +
		" text not null, " + COLUMN_NOTE +
		" text not null, " + COLUMN_IMAGE +
		" text not null);";

	public MySQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(MySQLiteHelper.class.getName(),
			"Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_EVENTS);
		onCreate(db);
	}
}
