package com.eco_skout;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.FrameLayout;
import android.view.Gravity;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.graphics.Color;

public class EcoSkout extends Activity
{
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		FrameLayout mainLayout = new FrameLayout(this);
		ImageView iv = new ImageView(this);
		iv.setScaleType(ImageView.ScaleType.FIT_XY);
		iv.setImageResource(R.drawable.splashscreen);
		LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
		iv.setLayoutParams(params);
		TextView power = new TextView(this);
		power.setText("Powered by Android");
		power.setLayoutParams(params);
		power.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM);
		power.setPadding(0, 0, 0, 80);
		TextView copyright = new TextView(this);
		copyright.setText("Copyright © 2018 Kalorjs");
		copyright.setLayoutParams(params);
		copyright.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM);
		copyright.setPadding(0, 0, 0, 50);
		mainLayout.addView(iv);
		mainLayout.addView(power);
		mainLayout.addView(copyright);
		setContentView(mainLayout);
	}
}