package com.eco_skout;

import android.database.sqlite.SQLiteDatabase;
import android.database.SQLException;
import android.database.Cursor;
import android.content.Context;
import android.content.ContentValues;
import java.util.List;
import java.util.ArrayList;
public class EventsDataSource
{
	private SQLiteDatabase database;
	private MySQLiteHelper dbHelper;
	private String[] allColumns = { MySQLiteHelper.COLUMN_ID, MySQLiteHelper.COLUMN_EVENT, MySQLiteHelper.COLUMN_DATE, MySQLiteHelper.COLUMN_LOCATION, MySQLiteHelper.COLUMN_ORGANIZER, MySQLiteHelper.COLUMN_DESCRIPTION, MySQLiteHelper.COLUMN_NOTE, MySQLiteHelper.COLUMN_IMAGE };

	public EventsDataSource(Context context) {
		dbHelper = new MySQLiteHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public void createEvent(String event, String date, String location, String organizer, String description, String note, String image) {
		ContentValues values = new ContentValues();
		values.put(MySQLiteHelper.COLUMN_EVENT, event);
		values.put(MySQLiteHelper.COLUMN_DATE, date);
		values.put(MySQLiteHelper.COLUMN_LOCATION, location);
		values.put(MySQLiteHelper.COLUMN_ORGANIZER, organizer);
		values.put(MySQLiteHelper.COLUMN_DESCRIPTION, description);
		values.put(MySQLiteHelper.COLUMN_NOTE, note);
		values.put(MySQLiteHelper.COLUMN_IMAGE, image);
		long insertId = database.insert(MySQLiteHelper.TABLE_EVENTS, null, values);
		Cursor cursor = database.query(MySQLiteHelper.TABLE_EVENTS,
			allColumns, MySQLiteHelper.COLUMN_ID + " = " + insertId, null,
			null, null, null);
		cursor.moveToFirst();
		cursor.close();
	}

	public void deleteEvent(Event event) {
		long id = event.getId();
		System.out.println("Event deleted with id: " + id);
		database.delete(MySQLiteHelper.TABLE_EVENTS,
			MySQLiteHelper.COLUMN_ID + " = " + id, null);
	}

	public ArrayList<Event> getAllEvents() {
		ArrayList<Event> events = new ArrayList<Event>();
		Cursor cursor = database.query(MySQLiteHelper.TABLE_EVENTS,
			allColumns, null, null, null, null, null);
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			Event event = cursorToEvent(cursor);
			events.add(event);
			cursor.moveToNext();
		}
		cursor.close();
		return events;
	}

	public Event cursorToEvent(Cursor cursor) {
		Event event = new Event();
		event.setId(cursor.getLong(0));
		event.setEvent(cursor.getString(1));
		event.setDate(cursor.getString(2));
		event.setLocation(cursor.getString(3));
		event.setOrganizer(cursor.getString(4));
		event.setDescription(cursor.getString(5));
		event.setNote(cursor.getString(6));
		event.setImage(cursor.getString(7));
		return event;
	}
}
