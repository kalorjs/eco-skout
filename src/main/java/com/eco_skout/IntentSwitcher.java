package com.eco_skout;

import android.view.View;
import android.content.Intent;
import android.content.Context;
import java.lang.Class;

public class IntentSwitcher implements View.OnClickListener
{
	Intent intent=null;
	Context cont=null;

	public IntentSwitcher(Context cont,Class<?> cls,String[] d) {
		intent=new Intent(cont.getApplicationContext(),cls);
		if(d!=null) {
			intent.putExtra("data",d);
		}
		this.cont=cont;
	}

	public void onClick(View view) {
		try {
			System.out.println("Button clicked");
			cont.startActivity(intent);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
}
