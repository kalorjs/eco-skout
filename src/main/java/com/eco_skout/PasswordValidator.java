package com.eco_skout;

public class PasswordValidator
{
	public static boolean isValid(String password) {
		if(password == null) {
			return(false);
		}
		if(password.length() < 8) {
			return(false);
		}
		if(password.equals(password.toLowerCase())) {
			return(false);
		}
		if(password.equals(password.toUpperCase())) {
			return(false);
		}
		if(password.matches("[A-Za-z0-9 ]*")) {
			return(false);
		}
		return(true);
	}

	public static boolean isMatch(String pw1, String pw2) {
		if(pw1.matches(pw2)) {
			return(true);
		}
		return(false);
	}
}
