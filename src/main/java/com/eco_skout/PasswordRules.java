package com.eco_skout;

import android.text.Editable;

public class PasswordRules
{
	static boolean isAtleastEight=false;
	static boolean hasUpper=false;
	static boolean hasLower=false;
	static boolean hasSpecial=false;

	public static void checkPassword(Editable s) {
		isAtleastEight=s.length()>=8?true:false;
		hasUpper=!s.toString().equals(s.toString().toLowerCase())?true:false;
		hasLower=!s.toString().equals(s.toString().toUpperCase())?true:false;
		hasSpecial=!s.toString().matches("[A-Za-z0-9 ]*")?true:false;
	}
}
