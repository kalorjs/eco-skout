package com.eco_skout;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.SearchView;

public class ScrollViewEvents extends Activity
{
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		LinearLayout l = new LinearLayout(this);
		l.setOrientation(LinearLayout.VERTICAL);
		ListOfEventLayout ll = new ListOfEventLayout();
		LinearLayout ml = new LinearLayout(this);
		ScrollView sv = new ScrollView(this);
		SearchView srv = new SearchView(this);
		srv.setQueryHint("Search for event title");
		srv.setIconified(false);
		ml.setOrientation(LinearLayout.VERTICAL);
		ml.addView(ll.eventLayout1(this, "Camping sa Mt.Jumbo", "JUN/22/2019", "La Trinidad", "John Emil"));
		ml.addView(ll.eventLayout1(this, "Batanes Escapade", "APR/22/2019", "Batanes", "John Emil"));
		ml.addView(ll.eventLayout1(this, "Sierra Madre Adventure", "DEC/22/2018", "Isabela", "John Emil"));
		ml.addView(ll.eventLayout1(this, "Camping sa Mt. Pulag", "OCT/05/2018", "La Trinidad", "John Emil"));
		ml.addView(ll.eventLayout1(this, "Mt. Ulap Hiking", "NOV/09/2018", "Itogon", "John Emil"));
		ml.addView(ll.eventLayout1(this, "Mt. Kalugong Adventure", "JAN/22/2018", "La Trinidad", "John Emil"));
		ml.addView(ll.eventLayout1(this, "Pugo Adventures", "FEB/22/2018", "Pugo, La Union", "John Emil"));
		ml.addView(ll.eventLayout1(this, "Tangadan Overnight Swimming", "MAR/22/2018", "San Gabriel, La Union", "John Emil"));
		ml.addView(ll.eventLayout1(this, "Camping sa Chico River", "JAN/22/2018", "Tabuk, Kalinga", "John Emil"));
		ml.addView(ll.eventLayout1(this, "Sierra Madre Tree Planting", "MAR/22/2018", "La Trinidad", "John Emil"));
		ml.addView(ll.eventLayout1(this, "Palaui Island Survival", "08/22/2018", "Sta. Ana", "John Emil"));
		ml.addView(ll.eventLayout1(this, "Boracay Outing", "08/22/2018", "Boracay Island", "John Emil"));
		sv.addView(ml);
		l.addView(srv);
		l.addView(sv);
		setContentView(l);
	}
}
