package com.eco_skout;

import android.app.Activity;
import android.app.ListActivity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.Button;
import android.widget.LinearLayout;
import android.view.View;
import java.util.List;
import java.util.Random;
import java.util.ArrayList;
import android.widget.ScrollView;
import android.widget.SearchView;
import android.content.Context;

public class EventFullDetailsActivity extends Activity
{
	private EventsDataSource datasource;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		LinearLayout mainLayout = new LinearLayout(this);
		mainLayout.setOrientation(LinearLayout.VERTICAL);
		ListOfEventLayout lel = new ListOfEventLayout();
		ArrayList<Event> values = datasource.getAllEvents();
		Event e = null;
		//id, event, date, location, organizer, description, note, image
		for(int i=0; i < values.size(); i++) {
			e = values.get(i);
			if(e.getEvent().equals("Pugo Adventures")) {
				mainLayout.addView(lel.eventFullLayout(this, e.getEvent(), e.getDate(), e.getLocation(), e.getOrganizer(), e.getDescription(), e.getNote(), e.getImage()));
			}
		}
		setContentView(mainLayout);
	}
}
