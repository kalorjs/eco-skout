package com.eco_skout;

import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Button;
import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.content.Intent;

public class ListOfEventLayout
{
	
	public LinearLayout eventLayout1(Context context, String event, String d, String location, String organizer) {
		String[] date = d.split("/");
		LinearLayout mainLayout = new LinearLayout(context);
		mainLayout.setOrientation(LinearLayout.VERTICAL);
		LinearLayout detailsLayout = new LinearLayout(context);
		detailsLayout.setOrientation(LinearLayout.HORIZONTAL);
		LinearLayout eventLayout = new LinearLayout(context);
		eventLayout.setOrientation(LinearLayout.VERTICAL);
		LinearLayout dateLayout = new LinearLayout(context);
		dateLayout.setOrientation(LinearLayout.VERTICAL);
		TextView tv1 = new TextView(context);
		TextView month = new TextView(context);
		TextView day = new TextView(context);
		TextView year = new TextView(context);
		TextView tv3 = new TextView(context);
		TextView tv4 = new TextView(context);
		tv1.setBackgroundColor(Color.argb(130, 0, 255, 0));
		dateLayout.setBackgroundColor(Color.argb(130, 0, 0, 255));
		detailsLayout.setBackgroundColor(Color.argb(130, 255, 255, 0));
		tv1.setText(event);
		// tv1.setClickable(true);
		// final Intent fd = new Intent(context, EventFullDetialsActivity.class);
		// tv1.setOnClickListener(new View.OnClickListener() {
		// 	public void onClick(View view) {
		// 	startActivity(fd);
		// 	}
		// });
		month.setText(date[0]);
		day.setText(date[1]);
		year.setText(date[2]);
		tv4.setText(organizer);
		tv3.setText(location);
		tv1.setTextSize(25);
		month.setTextSize(15);
		day.setTextSize(20);
		year.setTextSize(15);
		tv4.setTextSize(15);
		tv3.setTextSize(15);
		tv3.setLinksClickable(true);
		tv4.setLinksClickable(true);
		eventLayout.addView(tv3);
		eventLayout.addView(tv4);
		dateLayout.addView(month);
		dateLayout.addView(day);
		dateLayout.addView(year);
		detailsLayout.addView(dateLayout);
		detailsLayout.addView(eventLayout);
		mainLayout.addView(tv1);
		mainLayout.addView(detailsLayout);
		return mainLayout;
	}

	public LinearLayout eventFullLayout(Context context, String event, String d, String location, String organizer, String description, String note, String image) {
		String[] date = d.split("/");
		LinearLayout mainLayout = new LinearLayout(context);
		mainLayout.setOrientation(LinearLayout.VERTICAL);
		LinearLayout detailsLayout = new LinearLayout(context);
		detailsLayout.setOrientation(LinearLayout.HORIZONTAL);
		LinearLayout eventLayout = new LinearLayout(context);
		eventLayout.setOrientation(LinearLayout.VERTICAL);
		LinearLayout dateLayout = new LinearLayout(context);
		dateLayout.setOrientation(LinearLayout.VERTICAL);
		LinearLayout descriptionLayout = new LinearLayout(context);
		dateLayout.setOrientation(LinearLayout.VERTICAL);
		TextView tv1 = new TextView(context);
		TextView img = new TextView(context);
		TextView month = new TextView(context);
		TextView day = new TextView(context);
		TextView year = new TextView(context);
		TextView tv3 = new TextView(context);
		TextView tv4 = new TextView(context);
		TextView desc = new TextView(context);
		TextView nt = new TextView(context);
		// final Intent intent1 = new Intent(getApplicationContext(), OtherActivity.class);
		Button reg = new Button(context);
		reg.setText("Pre-register");
		// reg.setOnClickListener(new View.OnClickListener() {
		// 	public void onClick(View view) {
		// 	startActivity(intent1);
		// 	}
		// });
		tv1.setBackgroundColor(Color.argb(130, 0, 255, 0));
		dateLayout.setBackgroundColor(Color.argb(130, 0, 0, 255));
		detailsLayout.setBackgroundColor(Color.argb(130, 255, 255, 0));
		tv1.setText(event);
		month.setText(date[0]);
		day.setText(date[1]);
		year.setText(date[2]);
		tv4.setText(organizer);
		tv3.setText(location);
		tv1.setTextSize(25);
		month.setTextSize(15);
		day.setTextSize(20);
		year.setTextSize(15);
		tv4.setTextSize(15);
		tv3.setTextSize(15);
		tv3.setLinksClickable(true);
		tv4.setLinksClickable(true);
		eventLayout.addView(tv3);
		eventLayout.addView(tv4);
		dateLayout.addView(month);
		dateLayout.addView(day);
		dateLayout.addView(year);
		detailsLayout.addView(dateLayout);
		detailsLayout.addView(eventLayout);
		descriptionLayout.addView(desc);
		descriptionLayout.addView(nt);
		mainLayout.addView(tv1);
		mainLayout.addView(img);
		mainLayout.addView(detailsLayout);
		mainLayout.addView(descriptionLayout);
		mainLayout.addView(reg);
		return mainLayout;
	}
}
//https://bitbucket.org/kalorjs/eco-skout/pull-requests/new?source=eventlist&t=1
//		event = datasource.createEvent("event1" "08/22/2018", "loc1", "organizer 1", "description 1", "note 1", "image1");