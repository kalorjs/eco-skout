package com.eco_skout;

import android.os.Bundle;
import android.app.Activity;
import android.widget.LinearLayout;
import android.widget.EditText;
import android.widget.Button;
import android.content.Context;
import android.view.View;
import android.text.InputType;
import android.widget.TextView;
import android.text.TextWatcher;
import android.text.Editable;
import android.widget.PopupWindow;
import android.util.DisplayMetrics;
import android.content.Intent;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.view.ViewGroup;
import android.text.Editable;
import java.util.ArrayList;
import android.view.View.OnFocusChangeListener;

public class CamperRegistration extends Activity
{
	Popup popUp=null;
	private static int count=0;
	ArrayList<EditText> ets=new ArrayList<EditText>();
	boolean showned=false;

	public ArrayList<EditText> getTextFields(View v) {
		ViewGroup rootView=(ViewGroup)v;
		for(int i=0; i<rootView.getChildCount(); i++) {
			View v1=rootView.getChildAt(i);
			if(v1 instanceof ViewGroup) {
				getTextFields(v1);
				System.out.println("VIEWGROUP");
			}
			System.out.println("VIEW");
			if(v1 instanceof EditText) {
				EditText et=(EditText)v1;
				ets.add(et);
				System.out.println(et.getId());
			}
		}
		return ets;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_camper_reg);
		popUp=new Popup(this);
		final EditText fName=(EditText)findViewById(R.id.input_fName);
		final EditText lName=(EditText)findViewById(R.id.input_lName);
		final EditText bDate=(EditText)findViewById(R.id.input_date);
		final ImageButton dateBtn=(ImageButton)findViewById(R.id.date_btn);
		final RadioButton male=(RadioButton)findViewById(R.id.male);
		male.setChecked(true);
		final RadioButton female=(RadioButton)findViewById(R.id.female);
		final EditText mobile=(EditText)findViewById(R.id.input_number);
		final EditText email=(EditText)findViewById(R.id.input_email);
		final EditText password=(EditText)findViewById(R.id.input_pw1);
		final EditText password2=(EditText)findViewById(R.id.input_pw2);
		final Button submitBtn=(Button)findViewById(R.id.submitBtn);
		final RadioGroup radioGroup=(RadioGroup)findViewById(R.id.radioGroup);
		submitBtn.setEnabled(false);
		ArrayList<EditText> ets=getTextFields(getWindow().getDecorView());
		final int size=ets.size();
		System.out.println("Fucking size: "+size);

		for(int i=0; i<ets.size(); i++) {
			final EditText et=ets.get(i);
			et.setOnFocusChangeListener(new OnFocusChangeListener() {
				public void onFocusChange(View v,boolean hasFocus) {
					if(!hasFocus && !et.getText().toString().matches("")) {
						System.out.println(et.getText().toString());
						count++;
						System.out.println("Focus out");
					}
				}
			});
		}

		password2.addTextChangedListener(new TextWatcher(){
			public void afterTextChanged(Editable s) {
				if(s.length()>0) {
					count++;
					System.out.println(count);
					if(count>=size) {
						System.out.println("BUTTON ENABLED");
						submitBtn.setEnabled(true);
					}
				}
			}

			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
		});

		submitBtn.setOnClickListener(new View.OnClickListener() {
			StringBuilder message=new StringBuilder();
			boolean flag=false;
			public void onClick(View v) {
				System.out.println("CLICKED!!");
				int sexId=radioGroup.getCheckedRadioButtonId();
				String sex=sexId==male.getId()?"Male":"Female";
				String fn=fName.getText().toString();
				String ln=lName.getText().toString();
				String bd=bDate.getText().toString();
				String mob=mobile.getText().toString();
				String ea=email.getText().toString();
				String pass=password.getText().toString();
				String pass2=password2.getText().toString();
				boolean isEmailValid=EmailAddressValidator.isValid(ea);
				boolean isPasswordValid=PasswordValidator.isValid(pass);
				boolean passwordsMatch=PasswordValidator.isMatch(pass,pass2);
				System.out.println("haba"+message.length());
				if(!isEmailValid) {
					message.append("Invalid Email/");
					flag=true;
				}
				if(!isPasswordValid) {
					message.append("Invalid Password/");
					flag=true;
				}
				if(!passwordsMatch) {
					message.append("Passwords don't match/");
					flag=true;
				}
				if(!flag) {
					String[] details={"register",fn,ln,mob,ea,pass,bd,sex,"camper"};
					new HttpPOSTClient(EcoIP.ip+"register",popUp).execute(details);
				}
				else {
					message.append("ERROR");
					popUp.setMessage(message);
					popUp.showPopUp();
					popUp.setIsShown(true);
					System.out.println(message.toString());
					flag=false;
				}
				message.setLength(0);
				message=new StringBuilder();
				// showned=false;
			}
		});
	}
}
