package com.eco_skout;

import android.os.Bundle;
import android.app.Activity;
import android.widget.LinearLayout;
import android.widget.EditText;
import android.widget.Button;
import android.content.Context;
import android.view.View;
import android.text.InputType;
import android.widget.TextView;
import android.text.TextWatcher;
import android.text.Editable;
import android.widget.PopupWindow;
import android.util.DisplayMetrics;
import android.content.Intent;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.view.ViewGroup;
import java.util.ArrayList;
import android.view.View.OnFocusChangeListener;

public class OrganizerRegistration extends Activity
{
	Popup popUp=null;
	private static int count=0;
	ArrayList<EditText> ets=new ArrayList<EditText>();

	public ArrayList<EditText> getTextFields(View v) {
		ViewGroup rootView=(ViewGroup)v;
		for(int i=0; i<rootView.getChildCount(); i++) {
			View v1=rootView.getChildAt(i);
			if(v1 instanceof ViewGroup) {
				getTextFields(v1);
				System.out.println("VIEWGROUP PO ITETCH");
			}
			System.out.println("VIEW PO ITETCH");
			if(v1 instanceof EditText) {
				EditText et=(EditText)v1;
				ets.add(et);
				System.out.println(et.getId());
			}
		}
		return ets;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_organizer_reg);
		popUp=new Popup(this);
		final EditText companyName=(EditText)findViewById(R.id.comp_name);
		final EditText companyAddress=(EditText)findViewById(R.id.comp_address);
		final EditText companyEmail=(EditText)findViewById(R.id.comp_email);
		final EditText companyWebsite=(EditText)findViewById(R.id.comp_website);
		final EditText companyPermit=(EditText)findViewById(R.id.comp_permit);
		final Button nextBtn=(Button)findViewById(R.id.next_btn);
		nextBtn.setEnabled(false);
		ArrayList<EditText> ets=getTextFields(getWindow().getDecorView());
		final int size=ets.size();
		System.out.println("Fucking size: "+size);

		for(int i=0; i<ets.size(); i++) {
			final EditText et=ets.get(i);
			et.setOnFocusChangeListener(new OnFocusChangeListener() {
				public void onFocusChange(View v,boolean hasFocus) {
					if(!hasFocus && !et.getText().toString().matches("")) {
						System.out.println(et.getText().toString());
						count++;
						System.out.println("Focus out");
					}
				}
			});
		}

		companyPermit.addTextChangedListener(new TextWatcher() {
			public void afterTextChanged(Editable s) {
				if(s.length()>0) {
					count++;
					if(count>=size) {
						System.out.println("BUTTON ENABLED");
						nextBtn.setEnabled(true);
					}
				}
			}

			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
		});

		nextBtn.setOnClickListener(new View.OnClickListener() {
			Intent i=null;
			public void onClick(View v) {
			String cn=companyName.getText().toString();
			String ca=companyAddress.getText().toString();
			String ce=companyEmail.getText().toString();
			String cw=companyWebsite.getText().toString();
			String cp=companyPermit.getText().toString();
			i=new Intent(getApplicationContext(),SecondOrganizerRegistration.class);
			// final String[] details={cn,ca,ce,cw,cp};
			i.putExtra("cn",cn);
			i.putExtra("ca",ca);
			i.putExtra("ce",ce);
			i.putExtra("cw",cw);
			i.putExtra("cp",cp);
				startActivity(i);
			}
		});
	}
}
