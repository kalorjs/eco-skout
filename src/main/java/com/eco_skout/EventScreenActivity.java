package com.eco_skout;

import android.app.Activity;
import android.app.ListActivity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.Button;
// import android.widget.ListView;
import android.widget.LinearLayout;
// import android.widget.ArrayAdapter;
import android.view.View;
import java.util.List;
import java.util.Random;
import java.util.ArrayList;
import android.widget.ScrollView;
import android.widget.SearchView;

public class EventScreenActivity extends Activity
{
	private EventsDataSource datasource;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		LinearLayout l = new LinearLayout(this);
		l.setOrientation(LinearLayout.VERTICAL);
		ListOfEventLayout ll = new ListOfEventLayout();
		LinearLayout ml = new LinearLayout(this);
		ml.setOrientation(LinearLayout.VERTICAL);
		ScrollView sv = new ScrollView(this);
		SearchView srv = new SearchView(this);
		datasource = new EventsDataSource(this);
		datasource.open();
		Event event = null;
		//id, event, date, location, organizer, description, note, image
		datasource.createEvent("Mt. Kalugong Adventure", "JAN/22/2018", "La Trinidad", "John Emil", "", "", "");
		datasource.createEvent("Pugo Adventures", "FEB/22/2018", "Pugo, La Union", "John Emil", "", "", "");
		datasource.createEvent("Tangadan Overnight Swimming", "MAR/22/2018", "San Gabriel, La Union", "John Emil", "", "", "");
		datasource.createEvent("Camping sa Chico River", "JAN/22/2018", "Tabuk, Kalinga", "John Emil", "", "", "");
		datasource.createEvent("Sierra Madre Tree Planting", "MAR/22/2018", "La Trinidad", "John Emil", "", "", "");
		datasource.createEvent("Palaui Island Survival", "08/22/2018", "Sta. Ana", "John Emil", "", "", "");
		datasource.createEvent("Boracay Outing", "08/22/2018", "Boracay Island", "John Emil", "", "", "");
		datasource.createEvent("Camping sa Mt.Jumbo", "JUN/22/2019", "La Trinidad", "John Emil", "", "", "");
		datasource.createEvent("Sierra Madre Adventure", "DEC/22/2018", "Isabela", "John Emil", "", "", "");
		datasource.createEvent("Batanes Escapade", "APR/22/2019", "Batanes", "John Emil", "", "", "");
		datasource.createEvent("Camping sa Mt. Pulag", "OCT/05/2018", "La Trinidad", "John Emil", "", "", "");
		datasource.createEvent("Mt. Ulap Hiking", "NOV/09/2018", "Itogon", "John Emil", "", "", "");
		// datasource.createEvent("event6", "08/22/2018", "loc1", "organizer 1", "description 1", "note 1", "image1");
		// datasource.createEvent("event7", "08/22/2018", "loc1", "organizer 1", "description 1", "note 1", "image1");
		// datasource.createEvent("event8", "08/22/2018", "loc1", "organizer 1", "description 1", "note 1", "image1");
		ArrayList<Event> values = datasource.getAllEvents();
		Event e = null;
		// ListOfEventLayout ll = new ListOfEventLayout();
		for(int i=0; i < values.size(); i++) {
			e = values.get(i);
			ml.addView(ll.eventLayout1(this, e.getEvent(), e.getDate(), e.getLocation(), e.getOrganizer()));
		}
		sv.addView(ml);
		l.addView(srv);
		l.addView(sv);
		setContentView(l);
	}

	@Override
	protected void onResume() {
		datasource.open();
		super.onResume();
	}

	@Override
	protected void onPause() {
		datasource.close();
		super.onPause();
	}
}
