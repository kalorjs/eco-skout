package com.eco_skout;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.FrameLayout;
import android.view.ViewGroup.LayoutParams;
import android.view.Gravity;

public class SplashActivity extends Activity
{
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		FrameLayout mainLayout = new FrameLayout(this);
		final ImageView background = new ImageView(this);
		background.setImageResource(R.drawable.background);
		LinearLayout logoWrapper = new LinearLayout(this);
		final ImageView logo = new ImageView(this);
		logo.setImageResource(R.drawable.logo);
		logoWrapper.addView(logo);
		LinearLayout.LayoutParams logoParams = new LinearLayout.LayoutParams(1100, 500);
		logoParams.gravity = Gravity.CENTER;
		logo.setLayoutParams(logoParams);
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
		params.gravity = Gravity.CENTER;
		logoWrapper.setLayoutParams(params);
		mainLayout.addView(background);
		mainLayout.addView(logoWrapper);
		setContentView(mainLayout);
	}
}
