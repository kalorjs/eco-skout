package com.eco_skout;

public class EmailAddressValidator
{
	public static boolean isValid(String email) {
		if(email == null) {
			return(false);
		}
		if(email.contains(" ")) {
			return(false);
		}
		if(email.contains("@") == false) {
			return(false);
		}
		String domain = email.split("@")[1];
		if(domain.contains(".") == false) {
			return(false);
		}
		return(true);
	}
}
