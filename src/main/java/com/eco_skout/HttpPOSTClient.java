package com.eco_skout;

import android.os.AsyncTask;
import java.net.HttpURLConnection;
import java.net.URL;
import android.widget.TextView;
import org.json.JSONObject;
import android.util.JsonWriter;
import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class HttpPOSTClient extends AsyncTask<String, Void, String>
{
	Popup popUp=null;
	String webApi=null;

	public HttpPOSTClient(String w,Popup p) {
		webApi=w;
		popUp=p;
	}

	protected String doInBackground(String... str) {
		HttpURLConnection urlConnection=null;
		String responseData=null;
		try {
			URL url=new URL(webApi);
			urlConnection=(HttpURLConnection)url.openConnection();
			urlConnection.setDoOutput(true);
			urlConnection.setChunkedStreamingMode(0);
			OutputStream os=new BufferedOutputStream(urlConnection.getOutputStream());
			JsonWriter jWriter=new JsonWriter(new OutputStreamWriter(os,"UTF-8"));
			System.out.println("SERVERRRRRR");
			String[] fieldNames=
				{
					"",
					"fName",
					"lName",
					"mobile",
					"email",
					"password",
					"bDate",
					"sex",
					"uType",
					"comName",
					"comAddr",
					"comEmail",
					"comWeb",
					"comPer"
				};
			switch(str[0]) {
				case "login":
					jWriter.beginObject();
					jWriter.name("email").value(str[1]);
					jWriter.name("password").value(str[2]);
					jWriter.endObject();
					jWriter.close();
					break;
				case "register":
					System.out.println("nkapasok sa register");
					jWriter.beginObject();
					int len=str[8].matches("camper")?9:14;
					for(int i=1; i<len; i++) {
						if(len==14) {
							if(i==6 || i==7) {
								continue;
							}
						}
						System.out.println("writing"+fieldNames[i]+" as "+ str[i]);
						jWriter.name(fieldNames[i]).value(str[i]);
						System.out.println("written");
					}
					jWriter.endObject();
					jWriter.close();
					System.out.println("closed");
					// if("camper".equals(str[8])) {
					// 	System.out.println(str[8]);
					// 	jWriter.beginObject();
					// 	jWriter.name("fName").value(str[1]);
					// 	jWriter.name("lName").value(str[2]);
					// 	jWriter.name("bDate").value(str[3]);
					// 	jWriter.name("sex").value(str[4]);
					// 	jWriter.name("mobile").value(str[5]);
					// 	jWriter.name("email").value(str[6]);
					// 	jWriter.name("password").value(str[7]);
					// 	jWriter.endObject();
					// 	jWriter.close();
					// }
					break;
			}
			responseData=ServerResponseGetter.getResponse(urlConnection);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return responseData;
	}

	protected void onPostExecute(String result) {
		System.out.println("RESPONSE: "+result);
		StringBuilder resultText=new StringBuilder();
		try {
			JSONObject jObj=new JSONObject(result);
			System.out.println((String)jObj.get("result"));
			switch((String)jObj.get("result")) {
				case "success":
					// JSONObject jObj2=(JSONObject)jObj.get("value");
					// String idResult=(String)jObj2.get("_id");
					// String emailResult=(String)jObj2.get("email");
					// String passwordResult=(String)jObj2.get("password");
					// popUp.setMessage((String)jObj.get("message"));
					System.out.println("register successfull");
					resultText.append((String)jObj.get("message")+"/");
					resultText.append("SUCCESS");
					break;
				case "error":
					resultText.append((String)jObj.get("message")+"/");
					resultText.append("ERROR");
					break;
			}
			popUp.setMessage(resultText);
			popUp.showPopUp();
			System.out.println(resultText.toString());
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
}
