package com.eco_skout;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;

public class RegisterOptions extends Activity
{
	Button orgBtn=null;
	Button camperBtn=null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_options);
		camperBtn=(Button)findViewById(R.id.camperBtn);
		orgBtn=(Button)findViewById(R.id.organizerBtn);
		camperBtn.setOnClickListener(new IntentSwitcher(this,CamperRegistration.class,null));
		orgBtn.setOnClickListener(new IntentSwitcher(this,OrganizerRegistration.class,null));
	}
}
